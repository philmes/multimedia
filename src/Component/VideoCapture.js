import React, { useState, useRef } from 'react';
import MediaCapturer from 'react-multimedia-capture';

const VideoCapture = () => {
  const [video, setVideo] = useState({
    granted: false,
    rejectedReason: '',
    recording: false,
    paused: false
  });

  const appRef = useRef(null);

  const handleRequest = () => {
		console.log('Request Recording...');
	}
	const handleGranted = () =>  {
		setVideo({ granted: true });
		console.log('Permission Granted!');
	}
	const handleDenied = (err) =>   {
		setVideo({ rejectedReason: err.name });
		console.log('Permission Denied!', err);
	}
	const handleStart = (stream) => {
		setVideo({
			recording: true
		});
		setStreamToVideo(stream);
		console.log('Recording Started.');
  }
  
	const handleStop = (blob) => {
		setVideo({
			recording: false
		});
		releaseStreamFromVideo();
		console.log('Recording Stopped.');
		downloadVideo(blob);
  }
  
	const handlePause = () => {
		releaseStreamFromVideo();
		setVideo({
			paused: true
		});
  }
  
	const handleResume = (stream) => {
		setStreamToVideo(stream);
		setVideo({
			paused: false
		});
	}
	const handleError = (err) => {
		console.log(err);
	}
	const handleStreamClose = () => {
		setVideo({
			granted: false
		});
  }
  
  const setStreamToVideo = (stream) => {
		let video = appRef.current.querySelector('video');
		
		if(window.URL) {
      //video.src = window.URL.createObjectURL(stream);
      video.srcObject = stream;
		}
		else {
			video.src = stream;
		}
  }
  
  const releaseStreamFromVideo = () => {
		appRef.current.querySelector('video').src = '';
  }
  
	const downloadVideo = (blob) => {
		let url = URL.createObjectURL(blob);
		let a = document.createElement('a');
		a.style.display = 'none';
		a.href = url;
		a.target = '_blank';
		document.body.appendChild(a);

		a.click();
	}

  return (
    <div ref={appRef}>
				<h3>Video Recorder</h3>
				<MediaCapturer
					constraints={{ audio: false, video: true }}
					timeSlice={10}
					onRequestPermission={handleRequest}
					onGranted={handleGranted}
					onDenied={handleDenied}
					onStart={handleStart}
					onStop={handleStop}
					onPause={handlePause}
					onResume={handleResume}
					onError={handleError} 
					onStreamClosed={handleStreamClose}
					render={({ request, start, stop, pause, resume }) => 
					<div>
						<p>Granted: {'' + video.granted}</p>
						<p>Rejected Reason: {video.rejectedReason}</p>
						<p>Recording: {'' + video.recording}</p>
						<p>Paused: {'' + video.paused}</p>

						{!video.granted && <button onClick={request}>Get Permission</button>}
						<button onClick={start}>Start</button>
						<button onClick={stop}>Stop</button>
						<button onClick={pause}>Pause</button>
						<button onClick={resume}>Resume</button>
						
						<p>Streaming test</p>
						<video autoPlay></video>
					</div>
				} />
			</div>
  );
}
export default VideoCapture;