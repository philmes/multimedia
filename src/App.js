import './App.css';
import VideoCapture from './Component/VideoCapture';

function App() {
  return (
    <div className="App">
      <VideoCapture />
    </div>
  );
}

export default App;
